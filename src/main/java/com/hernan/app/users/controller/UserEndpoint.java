package com.hernan.app.users.controller;

public interface UserEndpoint {
	
	public final String  GET_USUARIOS ="/get-usuarios";
	public final String GET_USUARIO_BY_ID ="/get-usuario-by-id";
	public final String DELETE_USUARIOS = "/delete-all";
	public final String DELETE_USUARIO_BY_ID ="/delete-usuario-by-id";
	public final String UPDATE_USUARIO_BY_ID ="";
	public final String CREATE_USUARIO ="/create-usuario";
	public final String LOGIN ="/login-usuario";
	

}
