package com.hernan.app.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hernan.app.users.models.dtos.UsuarioDto;
import com.hernan.app.users.models.services.IUsuarioService;

@RestController
public class UsuarioController {
	
	@Autowired
	IUsuarioService usuarioService;
	
	
	@GetMapping(UserEndpoint.GET_USUARIOS)
	public List <UsuarioDto> listar(){
		return usuarioService.findAll();
	}
	
	@GetMapping(UserEndpoint.GET_USUARIO_BY_ID)
	public UsuarioDto getUsuariobyId(@RequestParam Long id) {
		return usuarioService.findById(id);
	}
	
	@GetMapping(UserEndpoint.DELETE_USUARIOS)
	public boolean deleteAll() {
		usuarioService.deleteAll();
		return true;
	}
	
	@GetMapping(UserEndpoint.DELETE_USUARIO_BY_ID)
	public boolean delete(@RequestParam Long id) {
		return usuarioService.delete(id);
	}
	@PostMapping(UserEndpoint.CREATE_USUARIO)
	public void delete(@RequestBody UsuarioDto usuarioDto) {
		usuarioService.create(usuarioDto);
	}
	
	@PostMapping(UserEndpoint.LOGIN)
	public boolean login(@RequestParam String  userName, @RequestParam String password) {
		return usuarioService.validateLogin(userName,password);
	}

}
