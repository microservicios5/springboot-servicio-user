package com.hernan.app.users.models.dtos;

import java.util.Date;

public class UsuarioDto {
	private Long id;
	private String name;
	private String userName;
	private Date createAt;
	private String password;
	
	public UsuarioDto() {}
	
	
	public UsuarioDto(Long id, String name, String userName, Date createAt) {
		this.id = id;
		this.name = name;
		this.userName = userName;
		this.createAt = createAt;
	}
	
	
	
	
	public UsuarioDto(Long id, String name, String userName, Date createAt, String password) {
		this.id = id;
		this.name = name;
		this.userName = userName;
		this.createAt = createAt;
		this.password = password;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	

}
