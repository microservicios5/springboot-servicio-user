package com.hernan.app.users.models.services;

import java.util.List;

import com.hernan.app.users.models.dtos.UsuarioDto;

public interface IUsuarioService {
	public List<UsuarioDto> findAll() ;
	
	public UsuarioDto findById(Long id);
	public void update(UsuarioDto usuarioDto);
	public void create(UsuarioDto usuarioDto);
	public boolean deleteAll();
	public boolean delete(Long id);
	public boolean validateLogin(String userName,String password);

}
