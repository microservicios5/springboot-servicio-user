package com.hernan.app.users.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hernan.app.users.models.entityes.Usuario;

public interface UserRepository extends JpaRepository<Usuario , Long > {
	
	@Query(value = "SELECT *FROM USUARIOS where user_name = ?1",nativeQuery = true)
	public Usuario findByUserName(String provincia); 

}
