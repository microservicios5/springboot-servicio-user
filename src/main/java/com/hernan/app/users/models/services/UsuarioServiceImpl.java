package com.hernan.app.users.models.services;

import java.util.List;
import java.util.stream.Collectors;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hernan.app.users.models.dtos.UsuarioDto;
import com.hernan.app.users.models.entityes.Usuario;
import com.hernan.app.users.models.repository.UserRepository;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public List<UsuarioDto> findAll() {
		List<Usuario> users = userRepository.findAll();
		List<UsuarioDto> asDto = users.stream().map(
		        s -> new UsuarioDto(s.getId(), s.getName(), s.getUserName() ,s.getCreateAt(),s.getPassword())
		).collect(Collectors.toList());
		
		return asDto; 
	} 

	@Override
	public UsuarioDto findById(Long id) {
		Usuario user = userRepository.findById(id).orElse(new Usuario() );
		return  new UsuarioDto(user.getId(), user.getName(),user.getUserName(),user.getCreateAt());
		
	}

	@Override
	public void update(UsuarioDto usuarioDto) {
		// TODO Auto-generated method stub
		if(!userRepository.existsById(usuarioDto.getId())) {
			
		}
	}

	@Override
	public void create(UsuarioDto usuarioDto) {
			String salt = BCrypt.gensalt();
			Usuario userNew = new Usuario();
			userNew.setCreateAt(usuarioDto.getCreateAt());
			userNew.setName(usuarioDto.getName());
			userNew.setPassword(BCrypt.hashpw(usuarioDto.getPassword(), salt));
			userNew.setUserName(usuarioDto.getUserName());
			userRepository.save(userNew);	
		
	}

	@Override
	public boolean deleteAll() {
		userRepository.deleteAll();
		return true;
	}

	@Override
	public boolean delete(Long id) {
		if(userRepository.existsById(id)) {
			userRepository.deleteById(id);
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean validateLogin(String userName, String password) {
		Usuario user= userRepository.findByUserName(userName);
		if(user != null) {
			String hashedPassword = user.getPassword();
			return BCrypt.checkpw(password, hashedPassword);
		}else {
			return false;
		}
		
	}

}
